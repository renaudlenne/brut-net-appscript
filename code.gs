function netWage(income, date = new Date()) {
  var data = {
    "dateToCheck": date.toISOString(),
    "calculationType":1,
    "name":"",
    "fiscalStatuteType":1,
    "children":0,
    "handicappedChildren":0,
    "applicantIsHandicapped":false,
    "personsMin65":0,
    "handicappedPersonsMin65":0,
    "personsPlus65":0,
    "handicappedPersonsPlus65":0,
    "nonRemarriedWidower":false,
    "partnerHandicapped":false,
    "partnerIncomeType":1,
    "partnerProfessionalIncomeIsUnderBareme":false,
    "partnerPensionIncomeIsUnderBareme":false,
    "partnerPensionIncomeIsUnderMaximumNetPension":false,
    "flemmishCommunity":false,
    "workStatuteType":"2",
    "nOSSType":2,
    "grossIncome":income,
    "workedDays":22,
    "workedHours":0,
    "maximumDays":22,
    "maximumHours":0,
    "receivedNonTaxableIncome":0
  };
  var options = {
    'method' : 'post',
    'contentType': 'application/json',
    // Convert the JavaScript object to a JSON string.
    'payload' : JSON.stringify(data)
  };
  var response = UrlFetchApp.fetch('https://gateways.acv-csc.be/grossnet/api/v1/GrossNet/determineNetWage', options);

  var json = response.getContentText();
  var data = JSON.parse(json);
  
  return data.netWage;
}

function doublePecule(input) {
  var data = {
    "Brutomaandloon": input
  };
  
  var options = {
    'method' : 'post',
    'contentType': 'application/json',
    // Convert the JavaScript object to a JSON string.
    'payload' : JSON.stringify(data)
  };
  var response = UrlFetchApp.fetch('https://www.sd.be/vakantiegeld/main/internet/API/BerekenVakantiegeld', options);

  var json = response.getContentText();
  var data = JSON.parse(json);
  
  return data.model.totaalNettoVakantiegeld;
}

function primeFinAnnee(input) {
  var data = {
    "pc":"200",
    "regio":"",
    "brutoMaand": input,
    "varWedde":"",
    "volledigJaar":"true",
    "aantalMaanden":"",
    "min60Dagen":"",
    "aantalKinderen":0
    };
  
  var options = {
    'method' : 'post',
    'contentType': 'application/json',
    // Convert the JavaScript object to a JSON string.
    'payload' : JSON.stringify(data)
  };
  var response = UrlFetchApp.fetch('https://www.sd.be/eindejaarspremie/dev/internet/API/BerekenEindejaarspremie', options);

  var json = response.getContentText();
  var data = JSON.parse(json);
  
  return data.model.nettoEindejaarspremie;
}
